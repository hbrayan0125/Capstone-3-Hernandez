import {Card, Container, Row, Col} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Link} from 'react-router-dom';

export default function OthersCard(){
    return (
        <>
            <Container style={{background: 'white'}} className='my-3 p-3'>
                <Row>
                    <Col xs={6} md={3} className='d-flex justify-content-center'>
                        <Card style={{ width: '22rem' ,borderRadius: "0px" }} className="me-3">
                            <Card.Body>
                                <Card.Title> Pet Shop Supplies </Card.Title>
                                <Card.Img style={{ borderRadius: "0px", height: "18rem" }} className="my-2" variant="top" src={ require("../images/pet-supplies.webp")} />
                                <Link style={{textDecoration: 'none'}} variant="primary">See More</Link>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col xs={6} md={3} className='d-flex justify-content-center'>
                        <Card style={{ width: '22rem' ,borderRadius: "0px" }} className="me-3">
                            <Card.Body>
                                <Card.Title> Fresh Home Ideas </Card.Title>
                                <Card.Img style={{ borderRadius: "0px", height: "18rem" }} className="my-2" variant="top" src={ require("../images/fresh-home-ideas.webp")} />
                                <Link style={{textDecoration: 'none'}} variant="primary">See More</Link>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col xs={6} md={3} className='d-flex justify-content-center'>
                        <Card style={{ width: '22rem' ,borderRadius: "0px" }} className="me-3">
                            <Card.Body>
                                <Card.Title>Shop Laptop & Tablets</Card.Title>
                                <Card.Img style={{ borderRadius: "0px", height: "18rem" }} className="my-2" variant="top" src={ require("../images/shop-laptop.webp")} />
                                <Link style={{textDecoration: 'none'}} variant="primary">See More</Link>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col xs={6} md={3} className='d-flex justify-content-center'>
                        <Card style={{ width: '22rem' ,borderRadius: "0px" }} className="me-3">
                            <Card.Body>
                                <Card.Title> Smart Watches</Card.Title>
                                <Card.Img style={{ borderRadius: "0px", height: "18rem" }} className="my-2" variant="top" src={ require("../images/smart-watches.webp")} />
                                <Link style={{textDecoration: 'none'}} variant="primary">See More</Link>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </>
    )
}