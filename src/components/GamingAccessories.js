import { Container, Image} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';

export default function GamingAccessories(){

    const responsive = {
        superLargeDesktop: {
          // the naming can be any, depends on you.
          breakpoint: { max: 4000, min: 1024 },
          items: 5
        },
        desktop: {
          breakpoint: { max: 1024, min: 800 },
          items: 3
        },
        tablet: {
          breakpoint: { max: 800, min: 464 },
          items: 2
        },
        mobile: {
          breakpoint: { max: 464, min: 0 },
          items: 1
        }
    }; 

    return (
        <>
            <Container style={{background: 'white'}} className='my-3 p-3'>
                <h2 className='text-center'>Gaming Accessories</h2>
                <Carousel responsive={responsive} className='pt-3'>
                    <div className='text-center'>
                        <Image style={{maxWidth: '12rem', maxHeight: '18rem'}} src={require("../images/gaming-accessories1.jpg")} rounded />
                        <p>Razer Huntsman V3 Pro</p>
                    </div>
                    <div className='text-center'>
                        <Image style={{maxWidth: '12rem', maxHeight: '18rem'}} src={require("../images/gaming-accessories2.jpg")} rounded />
                        <p>Razer x Dolce&Gabbana</p>
                    </div>
                    <div className='text-center'>
                        <Image style={{maxWidth: '12rem', maxHeight: '18rem'}} src={require("../images/gaming-accessories3.jpg")} rounded />
                        <p>Lenovo Legion 9i AI-tuned</p>
                    </div>
                    <div className='text-center'>
                        <Image style={{maxWidth: '12rem', maxHeight: '18rem'}} src={require("../images/gaming-accessories4.jpeg")} rounded />
                        <p>Audeze Maxwell Ultraviolet</p>
                    </div>
                    <div className='text-center'>
                        <Image style={{maxWidth: '12rem', maxHeight: '18rem'}} src={require("../images/gaming-accessories5.jpg")} rounded />
                        <p>Corsair K70 MAX</p>
                    </div>
                    <div className='text-center'>
                        <Image style={{maxWidth: '12rem', maxHeight: '18rem'}} src={require("../images/gaming-accessories6.jpeg")} rounded />
                        <p>Drop + The Lord of the Rings Ringwraith</p>
                    </div>
                </Carousel>
            </Container>
        </>
    )
}