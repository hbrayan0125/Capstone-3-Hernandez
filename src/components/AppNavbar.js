import { Navbar, Nav, NavDropdown,Container, Form, Offcanvas, Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Link} from 'react-router-dom';
import { useContext} from 'react';
import UserInfo from '../UserInfo';

// Link
import { NavLink } from 'react-router-dom';

export default function AppNavbar(){

    const {user}= useContext(UserInfo);
    const access = localStorage.getItem('token')


    return (
        <>
            <Navbar expand="lg">
            <Container>
                <Navbar.Brand as={Link} to="/">E-commerce-Website</Navbar.Brand>
                <Navbar.Toggle aria-controls="navbarScroll" />
                <Navbar.Collapse id="navbarScroll">
                    <Nav
                        className="me-auto my-2 my-lg-0"
                        style={{ maxHeight: '100px' }}
                        navbarScroll
                    >
                        
                        {/* <NavDropdown title="Categories" id="navbarScrollingDropdown">
                            <NavDropdown.Item href="#action3">Gadget</NavDropdown.Item>
                            <NavDropdown.Item href="#action4">
                                Home & Kitchen
                            </NavDropdown.Item>
                            <NavDropdown.Item href="#action4">
                                Health & Personal Care
                            </NavDropdown.Item>
                            <NavDropdown.Item href="#action4">
                                Pet Supplies
                            </NavDropdown.Item>
                            <NavDropdown.Item href="#action4">
                                Toys
                            </NavDropdown.Item>
                        </NavDropdown> */}
                        {
                            (access == undefined)?
                            <>
                                <Nav.Link as={Link} to="/">Home</Nav.Link>
                                <Nav.Link as={Link} to="/aboutus">About Us</Nav.Link>
                                <Nav.Link as={Link} to="/contactus">Contact Us</Nav.Link>
                            </>

                            :
                            <>
                                <Nav.Link as={Link} to="/">Home</Nav.Link>
                                <Nav.Link as={Link} to="/aboutus">About Us</Nav.Link>
                                <Nav.Link as={Link} to="/contactus">Contact Us</Nav.Link>
                                <Nav.Link as={Link} to="/userDashboard">Products</Nav.Link>
                                <Nav.Link as={Link} to="/userOrder">Order Details</Nav.Link>
                            </>
                            
                        }
                        
                    </Nav>
                    
                <Form className="d-flex">

                    {
                        (user.Firstname == undefined)?
                            <Link as={NavLink} to="/login" className="btn btn-success me-2" variant="success">Login</Link>
                        :
                            // <Nav.Link as={Link} to="/logout">Hello! {user.Firstname}</Nav.Link>
                            <>
                            <NavDropdown title={`Hello! ${user.Firstname}`} id="navbarScrollingDropdown">
                                <NavDropdown.Item href="#action4">
                                    Settings
                                </NavDropdown.Item>
                                <NavDropdown.Item as={Link} to="/logout">
                                    Logout
                                </NavDropdown.Item>
                                
                            </NavDropdown>
                            </>
                            
                    }
                    
                </Form>
                </Navbar.Collapse>
            </Container>
            </Navbar>
        </>
    )
}