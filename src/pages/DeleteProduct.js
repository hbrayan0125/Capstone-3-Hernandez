import React from 'react'; // Import React
import Button from '@mui/material/Button';

export default function DeleteProduct(props){

    const productId = props.product_id;
    const token = localStorage.getItem('token')

    const deleteProduct = (e, id) => {
        e.preventDefault();
        
        fetch('https://capstone2-hernandez-fg9k.onrender.com/products/archiveProduct', {
            method: 'POST',
            headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`
            },
            body: JSON.stringify({
            id
            }),
        }).then(res => res.json())
        .then(data => {
            console.log(data)
        })
    }
    
    return (
        <>
            <Button onClick={(e) => {deleteProduct(e, productId)}} style={{ color: "red", textDecoration: "none" }}>Delete</Button>
        </>
    )
}