import Carousel from 'react-bootstrap/Carousel';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../App.css';

// Cards
import Cards from '../components/Cards.js';
import GamingAccessories from '../components/GamingAccessories.js';
import TopSellerKitchen from '../components/TopSellerKitchen.js';
import OthersCard from '../components/OthersCard.js';
import Footer from '../pages/Footer'

export default function Home(){
    return (
        <>
           {/* Start Carousel Images */}
           <Carousel data-bs-theme="dark" className="custom-carousel mb-3">
                <Carousel.Item>
                    <img
                    className="d-block w-100 carousel-image"
                    src={ require("../images/new1.png")}
                    alt="First slide"
                    />
                </Carousel.Item>
                <Carousel.Item>
                    <img
                    className="d-block w-100 carousel-image"
                    src={ require("../images/new2.png")}
                    alt="Second slide"
                    />
                </Carousel.Item>
                <Carousel.Item>
                    <img
                    className="d-block w-100 carousel-image"
                    src={ require("../images/new3.png")}
                    alt="Third slide"
                    />
                </Carousel.Item>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src={ require("../images/new4.png")}
                    alt="Third slide"
                    />
                </Carousel.Item>
            </Carousel>
            {/* End Carousel Images */}

            {/* Start Cards */}
                <Cards />
            {/* End Cards */}

            {/* Start GamingAccessories */}
                <GamingAccessories />
            {/* End GamingAccessories */}

            {/* Start TopSellerKitchen */}
            <TopSellerKitchen />
            {/* End TopSellerKitchen */}

            {/* Start OthersCard */}
            <OthersCard />
            {/* End OthersCard */}

            {/* Start Footer */}
            <Footer />
            {/* End Footer */}
        </>
    )
}