import React, { useEffect } from 'react'; // Import React
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import TextField from '@mui/material/TextField';
import {useNavigate} from 'react-router-dom';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

export default function UpdateProduct(props){

    const navigate = useNavigate();

    const productId = props.product_id;
    const token = localStorage.getItem('token')

    const [Name, setName] = React.useState("");
    const [Description, setDescription] = React.useState("");
    const [Price, setPrice] = React.useState("");
    const [Stock, setStock] = React.useState("");

    const [open, setOpen] = React.useState(false);
    const handleOpen = () => {
        setOpen(true)
        fetch('https://capstone2-hernandez-fg9k.onrender.com/products/retrieveSingleProduct', {
            method: 'POST',
            headers: {
            'Content-Type': 'application/json'
            },
            body: JSON.stringify({
            id: productId
            }),
        }).then(res => res.json())
        .then((data) => {
            data.map(product => {
                setName(product.Name)
                setDescription(product.Description)
                setPrice(product.Price)
                setStock(product.Stock)
            })
        })
    }
    const handleClose = () => setOpen(false);

    const updateProduct = (e) => {
        e.preventDefault();

        fetch('https://capstone2-hernandez-fg9k.onrender.com/products/updateProduct', {
            method: 'POST',
            headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`
            },
            body: JSON.stringify({
            id: productId,
            Name,
            Description,
            Price,
            Stock
            }),
        }).then(res => res.json())
        .then(data => {
            setName(data.Name)
            setDescription(data.Description)
            setPrice(data.Price)
            setStock(data.Stock)
            handleClose()
            navigate('/adminDashboard')
        })

    }

    
        

    
    
    return (
        <>
            <Button style={{ color: "green", textDecoration: "none" }} onClick={handleOpen}>Update</Button>
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                <form onSubmit={(e) => {updateProduct(e)}}>
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        Update Product
                    </Typography>
                    <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                        <TextField className='mb-3' value={Name} onChange={(e) => setName(e.target.value)} fullWidth label="Product Name" id="fullWidth" />
                        <TextField className='mb-3' value={Description} onChange={(e) => setDescription(e.target.value)} fullWidth label="Product Description" id="fullWidth" />
                        <TextField className='mb-3' value={Price} onChange={(e) => setPrice(e.target.value)} fullWidth label="Product Price" id="fullWidth" />
                        <TextField className='mb-3' value={Stock} onChange={(e) => setStock(e.target.value)} fullWidth label="Product Stock" id="fullWidth" />
                    </Typography>
                    <Button type="submit" variant="contained" color="primary">
                        Submit
                    </Button>
                </form>
                </Box>
            </Modal>
        </>
    )
}