import { Navigate } from 'react-router-dom';
import UserInfo from '../UserInfo';
import {useContext, useEffect} from 'react';

export default function Logout() {

    const { unsetUser, setUser } = useContext(UserInfo);

    // updates localStorage to empty / clears the storage
    unsetUser();

    useEffect(()=>{
        setUser({id: null, isAdmin: null, Firstname: null, Lastname: null});
    })


    // Redirect back to login
    return (
        <Navigate to='/login' />
    )

}