import {Form, Row, Col, Container} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../App.css';
import {Link, useNavigate} from 'react-router-dom';
import {useContext, useState} from 'react';
import UserInfo from '../UserInfo';
import Swal from 'sweetalert2';


export default function Login(){

    const {user, setUser} = useContext(UserInfo);

    const navigate = useNavigate();

    const [Email, setEmail] = useState("");
    const [Password, setPassword] = useState("");

    const LoginUser = (e) => {
        e.preventDefault();

        fetch("https://capstone2-hernandez-fg9k.onrender.com/users/login", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                Email,
                Password,
            })
        }).then(res => res.json())
        .then(data => {
            console.log(data)
            if(data.message == false || data.incorrectPass == true){
                
                Swal.fire({
                    title: 'Login Failed!',
                    text: 'Please try again!',
                    icon: 'error',
                  })
                setEmail('');
                setPassword('');    
            }else{
                localStorage.setItem('token', data.access)

                getUserInfo(data.access);
            }
               
            
        })
    }

    const getUserInfo = (token) => {
        fetch("https://capstone2-hernandez-fg9k.onrender.com/users/order/RetrieveUserDetails", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`
            }
        }).then(res => res.json())
        .then(data => {

            setUser({
                id: data._id,
                isAdmin: data.isAdmin,
                Firstname: data.Firstname,
                Lastname: data.Lastname
            })

            if(data.isAdmin === true){
                Swal.fire({
                    title: 'Login Successfull!',
                    text: 'Welcome to Admin Dashboard!',
                    icon: 'success',
                  })
                navigate("/adminDashboard", {state: {Firstname: data.Firstname}});
            }else{
                Swal.fire({
                    title: 'Login Successfull!',
                    text: 'Welcome to User Dashboard!',
                    icon: 'success',
                  })
                navigate("/userDashboard", {state: {Firstname: data.Firstname}});
            }
            
        })
    }

    return (
        <>      
            <Container className="d-flex w-100 h-100 mt-3 justify-content-center" style={{padding: "5rem 5rem 6rem 5rem"}}>
                <Row className='w-50'>
                    <Col xs={6} md={8} className='p-5 w-100'>
                        <Form onSubmit={(e) => LoginUser(e)}>
                            <h3 className="mb-2 mt-0 text-3xl font-medium leading-tight text-primary text-center">Sign In</h3>
                            <Form.Group controlId="formBasicEmail" className='mb-3'>
                                <Form.Label>Email address</Form.Label>
                                <Form.Control
                                type="email"
                                value={Email}
                                onChange={(e) => setEmail(e.target.value)}
                                placeholder="Enter email"
                                />
                            </Form.Group>

                            <Form.Group controlId="formBasicPassword">
                                <Form.Label>Password</Form.Label>
                                <Form.Control
                                type="password"
                                value={Password}
                                onChange={(e) => setPassword(e.target.value)}
                                placeholder="Password"
                                />
                            </Form.Group>

                            <Form.Text className="mt-4 d-flex justify-content-start">
                                Forgot your password? 
                                <Link style={{textDecoration: 'none'}} className="ms-1" variant="primary" type="button">
                                Reset Password
                                </Link>
                            </Form.Text>

                            <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold mt-3 w-full py-2 px-4 rounded" type="submit">
                                Submit
                            </button>
                            
                            <Form.Text className="mt-2 d-flex justify-content-center">
                                Not a member?
                                <Link className="ms-1 text-primary transition duration-150 ease-in-out hover:text-primary-600 focus:text-primary-600 dark:text-primary-400 dark:hover:text-primary-500 dark:focus:text-primary-500" to={`/register`}>
                                Sign up now
                                </Link>
                            </Form.Text>
                        </Form>
                    </Col>
                </Row>
            </Container>
        </>
    )
}