import {Form, Row, Col, Container} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../App.css';
import {Link, useNavigate} from 'react-router-dom';
import {useState, useContext} from 'react';
import Swal from 'sweetalert2';
import UserInfo from '../UserInfo';

export default function Register(){

    const {user, setUser} = useContext(UserInfo);
    const [Email, setEmail] = useState("");
    const [Password, setPassword] = useState("");
    const [Firstname, setFirstName] = useState("");
    const [Lastname, setLastname] = useState("");

    const navigate = useNavigate();

   
    const AddUser = (e) => {
        e.preventDefault();
        
        fetch('https://capstone2-hernandez-fg9k.onrender.com/users/registerUser', {
            method: 'POST',
            headers: {
            'Content-Type': 'application/json',
            },
            body: JSON.stringify({
            Email,
            Password,
            Firstname,
            Lastname
            }),
        }).then(res => res.json())
        .then(data => {
            console.log(data)
            if(data.success == false){
                Swal.fire({
                    title: "Registration Failed",
                    text: `${data.message}`,
                    icon: 'error',
                  })
            }else{
                Swal.fire({
                    title: "Registration Successful!",
                    text: "Welcome to User Dashboard",
                    icon: 'success',
                  })

                localStorage.setItem('token', data.access)

                getUserInfo(data.access);
            }
            setEmail('');
            setPassword('');
            setFirstName('');
            setLastname('');
        })
    }

    const getUserInfo = (token) => {
        fetch("https://capstone2-hernandez-fg9k.onrender.com/users/order/RetrieveUserDetails", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`
            }
        }).then(res => res.json())
        .then(data => {

            setUser({
                id: data._id,
                isAdmin: data.isAdmin,
                Firstname: data.Firstname,
                Lastname: data.Lastname
            })

            if(data.isAdmin === true){
                Swal.fire({
                    title: 'Login Successfull!',
                    text: 'Welcome to Admin Dashboard!',
                    icon: 'success',
                  })
                navigate("/adminDashboard", {state: {Firstname: data.Firstname}});
            }else{
                navigate("/userDashboard", {state: {Firstname: data.Firstname}});
            }
            
        })
    }

    


    return (
        <>      
            <Container className="d-flex w-100 h-100 justify-content-center" style={{padding: "1rem 5rem 2rem 5rem"}}>
                <Row className='w-50'>
                    <Col xs={6} md={8} className='p-5 w-100'>
                        <Form onSubmit={(e) => AddUser(e)}>
                            <h3 className="mb-2 mt-0 text-3xl font-medium leading-tight text-primary text-center">Create Account</h3>
                            <Form.Group controlId="formBasicEmail" className='mb-3'>
                                <Form.Label>Email address</Form.Label>
                                <Form.Control
                                type="email"
                                value={Email}
                                onChange={(e) => setEmail(e.target.value)}
                                placeholder="Enter email"
                                />
                            </Form.Group>

                            <Form.Group controlId="formBasicPassword" className='mb-3'>
                                <Form.Label>Password</Form.Label>
                                <Form.Control
                                type="password"
                                value={Password}
                                onChange={(e) => setPassword(e.target.value)}
                                placeholder="Password"
                                />
                            </Form.Group>

                            <Form.Group controlId="formBasicFirstName" className='mb-3'>
                                <Form.Label>First Name:</Form.Label>
                                <Form.Control
                                type="text"
                                value={Firstname}
                                onChange = {(e) => setFirstName(e.target.value)}
                                placeholder="Enter First Name"
                                />
                            </Form.Group>

                            <Form.Group controlId="formBasicLastName" className='mb-3'>
                                <Form.Label>Last Name:</Form.Label>
                                <Form.Control
                                type="text"
                                value={Lastname}
                                onChange={(e) => setLastname(e.target.value)}
                                placeholder="Enter Last Name"
                                />
                            </Form.Group>

                            <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold mt-3 w-full py-2 px-4 rounded" type="submit">
                                Register Now
                            </button>
                            
                            <Form.Text className="mt-2 d-flex justify-content-center">
                                Already have an account?
                                <Link className="ms-1 text-primary transition duration-150 ease-in-out hover:text-primary-600 focus:text-primary-600 dark:text-primary-400 dark:hover:text-primary-500 dark:focus:text-primary-500" to={`/login`}>
                                Login now
                                </Link>
                            </Form.Text>
                        </Form>
                    </Col>
                </Row>
            </Container>
        </>
    )
}